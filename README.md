# OpenML dataset: ATLAS-Higgs-Boson-Machine-Learning-Challenge-2014

https://www.openml.org/d/45550

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is the datasets from the Kaggle Higgs Boson Machine Learning Challenge 2014. The data was downloaded from the [CERN website](http://opendata.cern.ch/record/328), which also hosts the documentation of the data. 
Further information about the challenge can be found on [Kaggle](https://www.kaggle.com/competitions/higgs-boson/), [the challenge website](https://higgsml.ijclab.in2p3.fr), and the [PMLR competition proceedings](http://proceedings.mlr.press/v42/). 
**Note:** This version encodes -999 as NaN.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45550) of an [OpenML dataset](https://www.openml.org/d/45550). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45550/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45550/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45550/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

